package it.bottega52.sonartest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/dummy")
public class DummyController {

  @GetMapping(headers = "Accept=application/json")
  public ResponseEntity<String> getDummy() {
    return new ResponseEntity<>("ciao!", HttpStatus.OK);
  }

}
