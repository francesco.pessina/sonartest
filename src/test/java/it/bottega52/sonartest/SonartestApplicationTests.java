package it.bottega52.sonartest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SonartestApplicationTests {

	@Test
	@Disabled
	void contextLoads() {
	}

}
